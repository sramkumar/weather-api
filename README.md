weather-api
===========

A Symfony project created on April 5, 2019, 11:38 pm.

```
composer install

php bin/console server:start 127.0.0.1:8000    -   To Start the server without virtual host (then go to http://127.0.0.1:8000
```
>Api Route to get weather data for a city
```
127.0.0.1:8000/api/weather/{city}
e.g
http://127.0.0.1:8000/api/weather/london