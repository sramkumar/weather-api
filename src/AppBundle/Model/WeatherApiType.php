<?php


namespace AppBundle\Model;

/**
 * Constant API type used, we can add more API types and return the class from weatherService in future
 * if any new API's are added.
 * Class WeatherApiType
 * @package AppBundle\Model
 */
class WeatherApiType
{
    const OPEN_WEATHER_MAP = 'open_weather_map';
    const YAHOO_WEATHER_MAP = 'yahoo_weather_map';
}