<?php


namespace AppBundle\Service;

use AppBundle\Model\WeatherApiType;
use AppBundle\Entity\OpenWeatherMap;
use AppBundle\Response\WeatherResponse;

class WeatherService
{
    protected $apiClient;

    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param $city
     * @param $type
     * @return WeatherResponse
     */
    public function getWeatherDetails($city, $type)
    {
        $weatherMapInstance = $this->getInstance($type);
        $apiData = $this->apiClient->getApiResponse($city);
        if($apiData->cod == 404){
            return $apiData->message;
        }
        try {
            $weatherMap = $weatherMapInstance->mapOpenWeatherMap($apiData);
        }catch (\Exception $e){
            return $e->getMessage();
        }
        return new WeatherResponse($weatherMap);
    }

    public function getInstance($type)
    {
        switch ($type){
            case WeatherApiType::OPEN_WEATHER_MAP:
            default:
                $weatherMapInstance = new OpenWeatherAdapter(new OpenWeatherMap());
        }

        return $weatherMapInstance;
    }
}