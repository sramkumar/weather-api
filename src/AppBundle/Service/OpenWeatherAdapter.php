<?php


namespace AppBundle\Service;

use AppBundle\Entity\OpenWeatherMap;

class OpenWeatherAdapter
{
    protected $openWeatherMap;

    public function __construct(OpenWeatherMap $openWeatherMap)
    {
        $this->openWeatherMap = $openWeatherMap;
    }

    public function mapOpenWeatherMap($data)
    {
        $this->openWeatherMap->setWeatherType(($data->weather[0]->main)??'');
        $this->openWeatherMap->setTemperature(($data->main->temp)??'');
        $this->openWeatherMap->setWindDegree(($data->wind->deg) ?? '');
        $this->openWeatherMap->setWindSpeed(($data->wind->speed)?? '');
        $direction = ($data->wind->deg ? $this->windDirection($data->wind->deg) : '');
        $this->openWeatherMap->setWindDirection($direction);
        return $this->openWeatherMap;
    }

    /**
     * Get wind direction from the degree
     * @param $windDegree
     * @return mixed|string
     */
    public function windDirection($windDegree)
    {
        $direction = '';

        $directions = [
            'North' => [348.75, 360],
            'North2' => [0, 11.25],
            'North-Northeast' => [11.25, 33.75],
            'Northeast' => [33.75, 56.25],
            'East-Northeast' => [56.25, 78.75],
            'East' => [78.75, 101.25],
            'East-Southeast' => [101.25, 123.75],
            'Southeast' => [123.75, 146.25],
            'South-Southeast' => [146.25, 168.75],
            'South' => [168.75, 191.25],
            'South-Southwest' => [191.25, 213.75],
            'Southwest' => [213.75, 236.25],
            'West-Southwest' => [236.25, 258.75],
            'West' => [258.75, 281.25],
            'West-Northwest' => [281.25, 303.75],
            'Northwest' => [303.75, 326.25],
            'North-Northwest' => [326.25, 348.75]
        ];

        foreach ($directions as $degree => $windAngle) {
            if ($windDegree >= $windAngle[0] && $windDegree < $windAngle[1]) {
                $direction = str_replace("2", "", $degree);
            }
        }
        return $direction;
    }
}