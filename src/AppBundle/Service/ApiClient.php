<?php


namespace AppBundle\Service;

class ApiClient
{
    protected  $api;

    /**
     * ApiClient constructor.
     * @param $api
     */
    public function __construct($api)
    {
        $this->api = $api;
    }

    public function getApiAbsoluteUrl($city)
    {
        return $this->api['endpoint']."?q=" . $city . "&appid=" . $this->api['appid'];
    }

    public function getApiResponse($city)
    {
        $apiUrl = $this->getApiAbsoluteUrl($city);
        return $this->apiResponse($apiUrl);
    }

    /**
     * Initialize CURL request to the API endpoint with required parameters
     * @param $apiUrl
     * @return mixed
     */
    public function apiResponse($apiUrl)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        curl_close($ch);
        $data = json_decode($response);

        return $data;
    }

}