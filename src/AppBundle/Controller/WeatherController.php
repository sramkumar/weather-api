<?php


namespace AppBundle\Controller;

use AppBundle\Model\WeatherApiType;
use AppBundle\Response\WeatherResponse;
use AppBundle\Service\WeatherService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOS;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class WeatherController extends AbstractFOSRestController
{
    /** @var WeatherService */
    protected  $weatherService;
    /**
     * WeatherController constructor.
     * @param WeatherService $weatherService
     */
    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    /**
     * @Method("GET")
     * @param city
     * @FOS\Route("/weather/{city}", methods={"GET"}, requirements={"city"="[a-zA-Z]+"}))
     * @FOS\View(
     *     serializerGroups={"Default", "Detailed"}
     * )
     * @return WeatherResponse
     */
    public function getWeatherDetailsAction($city)
    {
        return $this->weatherService->getWeatherDetails($city, WeatherApiType::OPEN_WEATHER_MAP);
    }
}