<?php


namespace AppBundle\Entity;

/**
 * Class OpenWeatherMap
 * @package AppBundle\Entity
 */
class OpenWeatherMap implements WeatherInterface
{
    protected $weatherType;

    protected $temperature;

    protected $windSpeed;

    protected $windDegree;

    protected $windDirection;

    /**
     * @return mixed
     */
    public function weatherType()
    {
        return $this->weatherType;
    }

    /**
     * @param mixed $weatherType
     */
    public function setWeatherType($weatherType): void
    {
        $this->weatherType = $weatherType;
    }

    /**
     * @return mixed
     */
    public function temperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $temperature
     */
    public function setTemperature($temperature): void
    {
        $this->temperature = $temperature;
    }

    /**
     * @return mixed
     */
    public function windSpeed()
    {
        return $this->windSpeed;
    }

    /**
     * @param mixed $windSpeed
     */
    public function setWindSpeed($windSpeed): void
    {
        $this->windSpeed = $windSpeed;
    }

    /**
     * @return mixed
     */
    public function windDegree()
    {
        return $this->windDegree;
    }

    /**
     * @param mixed $windDegree
     */
    public function setWindDegree($windDegree): void
    {
        $this->windDegree = $windDegree;
    }

    /**
     * @return mixed
     */
    public function windDirection()
    {
        return $this->windDirection;
    }

    /**
     * @param mixed $windDirection
     */
    public function setWindDirection($windDirection): void
    {
        $this->windDirection = $windDirection;
    }
}