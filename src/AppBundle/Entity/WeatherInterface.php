<?php


namespace AppBundle\Entity;


interface WeatherInterface
{
    /**
     * Get Weather Type attribute
     * @return mixed
     */
    public function weatherType();

    /**
     * Get Temperature
     * @return mixed
     */
    public function temperature();

    /**
     * Get wind Speed
     * @return mixed
     */
    public function windSpeed();

    /**
     * Get wind direction
     * @return mixed
     */
    public function windDirection();
}