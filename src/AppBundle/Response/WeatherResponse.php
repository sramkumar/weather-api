<?php


namespace AppBundle\Response;

use AppBundle\Entity\WeatherInterface;
use JMS\Serializer\Annotation as JMS;

/**
 * Class ReleaseResponse.
 *
 * @JMS\ExclusionPolicy("ALL")
 */
class WeatherResponse
{
    /**
     * @var WeatherInterface
     */
    private $weather;

    /**
     * WeatherResponse constructor.
     * @param WeatherInterface $weather
     */
    public function __construct(WeatherInterface $weather)
    {
        $this->weather = $weather;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getWeatherType(): string
    {
        return $this->weather->weatherType();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getTemperature(): string
    {
        return $this->weather->temperature();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getWindSpeed(): string
    {
        return $this->weather->windSpeed();
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\Type("string")
     * @JMS\Groups({"Default"})
     * @JMS\Since("1.0")
     *
     * @return string
     */
    public function getWindDirection(): string
    {
        return $this->weather->windDirection();
    }
}